package com.example.linkup.utils

import android.graphics.Color
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.linkup.R
import java.util.*

class CalendarAdapter(
    var daysOfMonth: MutableList<DateOption>,
    private val onItemListener: OnItemListener
) :
    RecyclerView.Adapter<CalendarAdapter.ViewHolder>() {

    interface OnItemListener {
        fun onItemClick(position: Int, dayText: String)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val dayOfMonth: TextView = itemView.findViewById(R.id.cellDayText)
        private val score: TextView = itemView.findViewById(R.id.dateScore)

        @RequiresApi(Build.VERSION_CODES.O)
        fun bind(position: Int) {
            if (daysOfMonth[position].date == null) {
                dayOfMonth.text = ""
                score.text = ""
            } else {
                dayOfMonth.text = daysOfMonth[position].date.dayOfMonth.toString()
                score.text = Arrays.stream(daysOfMonth[position].slotScores).max().asInt.toString()

                if (score.text.toString() != "0") {
                    val old = score.text.toString()
                    score.setTextColor(Color.parseColor("#FFFF6624"))
                    score.text = old
                }
            }
            itemView.setOnClickListener {
                onItemListener.onItemClick(position, dayOfMonth.text.toString())
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.calendar_cell, parent, false)
        val layoutParams = view.layoutParams
        layoutParams.height = ((parent.height * 0.166666666).toInt())
        return ViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int = daysOfMonth.size
}