package com.example.linkup.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class User {

    private final int userId;
    private String userName;
    private List<Integer> thisWeekLinkUpIds;
    private List<String> thisWeekLinkUpNames;
    private List<Integer> laterWeeksLinkUpIds;
    private List<String> laterWeeksLinkUpNames;
    private List<Integer> dateTBDLinkUpIds;
    private List<String> dateTBDLinkUpNames;

    private final Database db;

    public User(int userId) {
        this.userId = userId;
        this.db = new Database();
    }

    public String generateUserName() {
        if (userName == null) {
            String sqlStatement = "SELECT user_name\n" +
                    "FROM public.\"Users\"\n" +
                    "WHERE user_id = " + userId;
            ResultSet resultSet = null;
            try {
                resultSet = db.query(sqlStatement);
                if (resultSet.next())
                    return resultSet.getString("user_name");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } finally {
                if (resultSet != null) {
                    try {
                        resultSet.close();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        }
        return userName;
    }

    public List<Integer> generateFriendsIds() {
        ResultSet resultSet = null;
        List<Integer> friends = new ArrayList<>();
        String sqlStatement = "SELECT friend_id \n" +
                    "FROM public.\"Friendships\"" +
                    "WHERE user_id = " + userId;
        try {
            resultSet = db.query(sqlStatement);
            while (resultSet.next()) {
                int i = resultSet.getInt("friend_id");
                friends.add(i);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }
        return friends;
    }

    public List<String> generateFriendsNames() {
        ResultSet resultSet = null;
        List<String> friendsNames = new ArrayList<>();
        List<Integer> friendsIds = generateFriendsIds();
        for (Integer friendId : friendsIds) {
            String sqlStatement = "SELECT user_name FROM public.\"Users\" WHERE user_id = " + friendId;
            try {
                resultSet = db.query(sqlStatement);
                resultSet.next();
                String s = resultSet.getString("user_name");
                friendsNames.add(s);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } finally {
                if (resultSet != null) {
                    try {
                        resultSet.close();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        }
        return friendsNames;
    }

    public boolean isGoingToLinkUp(int linkup_id) {
        ResultSet resultSet = null;
        String sqlStatement = "SELECT is_going FROM public.\"Attending\" WHERE user_id = " + userId
                + " AND linkup_id = " + linkup_id;
        try {
            resultSet = db.query(sqlStatement);
            if (resultSet.next())
                return resultSet.getBoolean("is_going");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }
        return false;
    }

    private List<Integer> generateLinkUpIds(boolean thisWeek, boolean nextWeek) {
        String sqlStatement;
        ResultSet resultSet = null;
        List<Integer> linkUpIds = new ArrayList<>();
        if (thisWeek) {
            sqlStatement = "SELECT public.\"Attending\".linkup_id \n" +
                    "FROM public.\"Attending\" NATURAL JOIN public.\"LinkUps\"  \n" +
                    "WHERE user_id = " + userId + "\n" +
                    "AND public.\"LinkUps\".date >= CURRENT_DATE\n" +
                    "AND date_trunc('week', public.\"LinkUps\".date) = date_trunc('week', CURRENT_DATE)";

        } else if (nextWeek) {
            sqlStatement = "SELECT public.\"Attending\".linkup_id \n" +
                    "FROM public.\"Attending\" NATURAL JOIN public.\"LinkUps\"  \n" +
                    "WHERE user_id = " + userId + "\n" +
                    "AND public.\"LinkUps\".date >= CURRENT_DATE\n" +
                    "AND NOT (date_trunc('week', public.\"LinkUps\".date) = date_trunc('week', CURRENT_DATE))";
        } else {
            sqlStatement = "SELECT public.\"Attending\".linkup_id \n" +
                    "FROM public.\"Attending\" NATURAL JOIN public.\"LinkUps\"  \n" +
                    "WHERE user_id = " + userId + "\n" +
                    "AND public.\"LinkUps\".date IS NULL\n";
        }
        try {
            resultSet = db.query(sqlStatement);
            while (resultSet.next()) {
                int i = resultSet.getInt("linkup_id");
                linkUpIds.add(i);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }
        return linkUpIds;
    }

    public List<Integer> generateThisWeekLinkUpIds() {
        if (thisWeekLinkUpIds == null) {
            thisWeekLinkUpIds = generateLinkUpIds(true, false);
        }
        return thisWeekLinkUpIds;
    }

    public List<Integer> generateLaterWeeksLinkUpIds() {
        if (laterWeeksLinkUpIds == null) {
            laterWeeksLinkUpIds = generateLinkUpIds(false, true);
        }
        return laterWeeksLinkUpIds;
    }

    private List<String> generateLinkUpNames(boolean thisWeek, boolean nextWeek) {
        ResultSet resultSet = null;
        List<String> linkUpNames = new ArrayList<>();
        List<Integer> linkUpIds;
        if (thisWeek) {
            linkUpIds = generateThisWeekLinkUpIds();
        } else if (nextWeek){
            linkUpIds = generateLaterWeeksLinkUpIds();
        } else {
            linkUpIds = generateDateTBDLinkUpIds();
        }
        for (Integer linkup_id : linkUpIds) {
            String sqlStatement = "SELECT linkup_name FROM public.\"LinkUps\" WHERE linkup_id = " + linkup_id;
            try {
                resultSet = db.query(sqlStatement);
                resultSet.next();
                String s = resultSet.getString("linkup_name");
                linkUpNames.add(s);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } finally {
                if (resultSet != null) {
                    try {
                        resultSet.close();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        }
        return linkUpNames;
    }

    public List<String> generateThisWeekLinkUpNames() {
        if (thisWeekLinkUpNames == null) {
            thisWeekLinkUpNames = generateLinkUpNames(true, false);
        }
        return thisWeekLinkUpNames;
    }

    public List<String> generateLaterWeeksLinkUpNames() {
        if (laterWeeksLinkUpNames == null) {
            laterWeeksLinkUpNames = generateLinkUpNames(false, true);
        }
        return laterWeeksLinkUpNames;
    }

    public void destroyUser() {
            db.close();
    }

    public List<String> generateDateTBDLinkUpNames() {
        if (dateTBDLinkUpNames == null) {
            return generateLinkUpNames(false, false);
        }
        return dateTBDLinkUpNames;
    }

    public List<Integer> generateDateTBDLinkUpIds() {
        if (dateTBDLinkUpIds == null) {
            return generateLinkUpIds(false, false);
        }
        return dateTBDLinkUpIds;
    }

    public void addFriend(int participantId) {
        String sqlStatement = "INSERT INTO public.\"Friendships\" (user_id, friend_id)" +
                " VALUES (" + userId + ", " + participantId + "); \n" +
                "INSERT INTO public.\"Friendships\" (user_id, friend_id)" +
                " VALUES (" + participantId + ", " + userId + ");";
        db.execute(sqlStatement);
    }
}
