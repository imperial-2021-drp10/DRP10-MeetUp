package com.example.linkup.utils

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.linkup.R

class TimeslotListAdapter(
    private val dateOption: DateOption,
    private val timeslotListener: TimeslotListener
) : RecyclerView.Adapter<TimeslotListAdapter.ViewHolder>() {

    interface TimeslotListener {
        fun onTimeslotClick(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.timeslot_option, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int = dateOption.slotScores.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val timeslotName = itemView.findViewById<TextView>(R.id.timeslotTV)
        private val timeslotScore = itemView.findViewById<TextView>(R.id.timeslotScoreTV)

        @SuppressLint("SetTextI18n")
        fun bind(position: Int) {
            var startTime = position
            var endTime = position + 1
            val startPostfix: String
            val endPostfix: String
            if (startTime < 12 || startTime == 0) {
                if (startTime == 0) {
                    startTime = 12
                }
                startPostfix = "am"
            } else {
                startTime = if (startTime == 12) {startTime} else {startTime - 12}
                startPostfix = "pm"
            }
            if (endTime < 12 || endTime == 24) {
                if (endTime == 24) {
                    endTime -= 12
                }
                endPostfix = "am"
            } else {
                endTime = if (endTime == 12) {endTime} else {endTime - 12}
                endPostfix = "pm"
            }

            timeslotName.text = "$startTime$startPostfix - $endTime$endPostfix"
            timeslotScore.text = dateOption.slotScores[position].toString()

            itemView.setOnClickListener {
                timeslotListener.onTimeslotClick(position)
            }
        }
    }
}