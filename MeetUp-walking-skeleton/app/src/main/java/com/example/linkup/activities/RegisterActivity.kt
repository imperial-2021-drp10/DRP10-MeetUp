package com.example.linkup.activities

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.linkup.R
import com.example.linkup.utils.Database
import com.example.linkup.utils.HashFunction


class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        // Get the relevant views from the layout
        val emailField =
            findViewById<EditText>(R.id.registerEmail) // emails are converted to lowercase when dealing with database
        val nameField = findViewById<EditText>(R.id.registerDisplayName)
        val passwordField = findViewById<EditText>(R.id.registerPassword)
        val confirmPasswordField = findViewById<EditText>(R.id.registerConfirmPassword)
        val confirmRegistrationButton = findViewById<Button>(R.id.registerConfirmRegistrationButton)

        confirmRegistrationButton?.setOnClickListener {

            // Check that all fields are filled
            if (emailField.text.isBlank() || nameField.text.isBlank() || passwordField.text.isBlank() || confirmPasswordField.text.isBlank()) {
                // Notify user to fill all fields
                Toast.makeText(
                    this@RegisterActivity,
                    "Error: All fields must be completed",
                    Toast.LENGTH_LONG
                ).show()
            }
            // Check if the passwords match
            else if (passwordField.text.toString() != confirmPasswordField.text.toString()) {
                // Notify user that passwords don't match
                Toast.makeText(
                    this@RegisterActivity,
                    "Error: Passwords don't match",
                    Toast.LENGTH_LONG
                ).show()
            }
            // Upload the new user to the database
            else {

                // Get the values of each field
                val email = emailField.text.toString().lowercase()
                val name = nameField.text.toString()
                val password = HashFunction.hash(passwordField.text.toString())

                // Connect to the database
                val db = Database()

                // Set up a query
                val query = db.Query(
                    """
                        SELECT COUNT(user_email)
                        FROM public."Users"
                        WHERE user_email='$email'
                    """.trimIndent()
                )

                val emailCheckThread = Thread(query)

                emailCheckThread.start()
                emailCheckThread.join()

                val emailResults = query.result

                // Ensure that there was a query
                if (emailResults == null) {
                    Toast.makeText(
                        this@RegisterActivity,
                        "Error: Problem connecting to database",
                        Toast.LENGTH_LONG
                    ).show()
                }
                else {

                    emailResults.next()

                    // Check if there's more than one occurrence of the email
                    if (emailResults.getInt("count") > 0) {
                        // Tell the user an account with that email already exists
                        Toast.makeText(
                            this@RegisterActivity,
                            "Error: Email already exists",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    // Upload the user's details to the database
                    else {

                        val insertThread = Thread {
                            val statement = db.extraConnection.prepareStatement("""
                                    INSERT INTO public."Users"(user_email, user_name, user_password)
                                    VALUES ('$email', '$name', ?)
                                """.trimIndent())
                            statement.setString(1, password)
                            statement.executeUpdate()
                        }

                        insertThread.start()
                        insertThread.join()

                        Toast.makeText(
                            this@RegisterActivity,
                            "Registration complete",
                            Toast.LENGTH_LONG
                        ).show()
                        finish()

                    }

                }

            }

        }
    }
}