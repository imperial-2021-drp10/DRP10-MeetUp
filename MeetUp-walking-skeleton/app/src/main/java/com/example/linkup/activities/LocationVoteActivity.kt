package com.example.linkup.activities

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.linkup.R
import com.example.linkup.utils.LocationListAdapter
import com.example.linkup.utils.LocationVoteSystem

class LocationVoteActivity : AppCompatActivity() {

    private lateinit var adapter: LocationListAdapter
    private lateinit var locationVoteSystem: LocationVoteSystem
    private lateinit var locationList: RecyclerView
    private lateinit var scoreToWinText: TextView
    private var userId = -1
    private var linkupId = -1
    private var linkupSize = -1

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_vote)

        userId = intent.getIntExtra("USER_ID", -1)
        linkupId = intent.getIntExtra("MEETUP_ID", -1)
        linkupSize = intent.getIntExtra("MEETUP_SIZE", -1)

        if (intent.getParcelableExtra<LocationVoteSystem>("VOTE_SYSTEM") != null) {
            locationVoteSystem = intent.getParcelableExtra("VOTE_SYSTEM")!!
            Log.i("LocationVoteActivity", "got the vote system back")
            Log.i(
                "LocationVoteActivity",
                "scoreToWin: ${locationVoteSystem.getScoreToWin()}, userId: ${locationVoteSystem.getUserId()}"
            )
        } else {
            locationVoteSystem = LocationVoteSystem(linkupSize, userId, linkupId)
            Log.i(
                "LocationVoteActivity",
                "scoretoWin: ${locationVoteSystem.getScoreToWin()}, userId: ${locationVoteSystem.getUserId()}"
            )
        }

        locationList = findViewById(R.id.locationList)
        scoreToWinText = findViewById(R.id.scoreToWinText)
        if (locationVoteSystem.voteWon) {
            scoreToWinText.text = "Vote won!!!!"
        } else {
            scoreToWinText.text = "Score to win is ${locationVoteSystem.getScoreToWin()}!"
        }

        adapter = LocationListAdapter(
            this,
            locationVoteSystem.locations, locationVoteSystem.voteWon,
            object : LocationListAdapter.UpvoteListener {
                override fun onUpvoteClick(position: Int) {
                    updateSystemWithUpvote(position)
                }
            },
            object : LocationListAdapter.DownvoteListener {
                override fun onDownvoteClick(position: Int) {
                    updateSystemWithDownvote(position)
                }
            },
            object : LocationListAdapter.DescriptionListener {
                override fun onDescriptionClick(position: Int) {
                    viewDescription(position)
                }
            }
        )
        locationList.adapter = adapter
        locationList.layoutManager = LinearLayoutManager(this)
        Log.i("LocationVoteActivity", "list size is " + locationVoteSystem.locations.size)
    }

    private fun viewDescription(position: Int) {
        val descIntent = Intent(this, LocationDescriptionActivity::class.java).apply {
            putExtras(intent)
            putExtra("MEETUP_ID", linkupId)
            putExtra("LOCATION_ID", locationVoteSystem.locations[position].locationId)
        }
        startActivity(descIntent)
        finish()
    }

    @SuppressLint("SetTextI18n")
    private fun updateSystemWithDownvote(position: Int) {
        locationVoteSystem.addDownvote(position)
        if (locationVoteSystem.voteWon) {
            scoreToWinText.text = "Vote won!!!!"
        } else {
            scoreToWinText.text = "Score to win is ${locationVoteSystem.getScoreToWin()}!"
        }
        adapter.notifyDataSetChanged()
    }

    @SuppressLint("SetTextI18n")
    private fun updateSystemWithUpvote(position: Int) {
        locationVoteSystem.addUpvote(position)
        if (locationVoteSystem.voteWon) {
            scoreToWinText.text = "Vote won!!!!"
        } else {
            scoreToWinText.text = "Score to win is ${locationVoteSystem.getScoreToWin()}!"
        }
        adapter.notifyDataSetChanged()
    }

    @Suppress("UNUSED_PARAMETER")
    fun addOption(view: View) {
        val addIntent = Intent(this, AddLocationActivity::class.java).apply {
            putExtra("VOTE_SYSTEM", locationVoteSystem)
        }
        startActivity(addIntent)
        finish()
    }

    fun switchToInfoActivity(view: View) {
        val explanationIntent = Intent(this, VoteExplanationActivity::class.java)
        startActivity(explanationIntent)
        finish()
    }

//    override fun onBackPressed() {
//        val upcomingLinkupsIntent = Intent(this, LinkUpActivity::class.java)
//        upcomingLinkupsIntent.putExtras(intent)
//        startActivity(upcomingLinkupsIntent)
//        finish()
//    }
}