package com.example.linkup.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.linkup.R

class LocationListAdapter(
    private val context: Context,
    private val locations: MutableList<LocationOption>,
    private val voteWon: Boolean,
    private val upvoteListener: UpvoteListener,
    private val downvoteListener: DownvoteListener,
    private val descriptionListener: DescriptionListener
) :
    RecyclerView.Adapter<LocationListAdapter.ViewHolder>() {

    interface UpvoteListener {
        fun onUpvoteClick(position: Int)
    }

    interface DownvoteListener {
        fun onDownvoteClick(position: Int)
    }

    interface DescriptionListener {
        fun onDescriptionClick(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.location_option, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int = locations.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val locName = itemView.findViewById<TextView>(R.id.locName)
        private val locDesc = itemView.findViewById<TextView>(R.id.locDesc)
        private val locScore = itemView.findViewById<TextView>(R.id.locScore)
        private val upvoteButton = itemView.findViewById<ImageButton>(R.id.upvoteButton)
        private val downvoteButton = itemView.findViewById<ImageButton>(R.id.downvoteButton)

        fun bind(position: Int) {
            locName.text = locations[position].name
            locDesc.text = locations[position].description
            locScore.text = locations[position].score.toString()

            if (locations[position].voteStatus == LocationOption.VoteStatus.UPVOTED) {
                upvoteButton.setImageResource(R.drawable.ic_upvote_clicked)
            }

            if (locations[position].voteStatus == LocationOption.VoteStatus.DOWNVOTED) {
                downvoteButton.setImageResource(R.drawable.ic_downvote_clicked)
            }

            upvoteButton.setOnClickListener {
                if (locations[position].voteStatus == LocationOption.VoteStatus.UPVOTED) {
                    upvoteButton.setImageResource(R.drawable.ic_upvote)
                } else {
                    upvoteButton.setImageResource(R.drawable.ic_upvote_clicked)
                }
                downvoteButton.setImageResource(R.drawable.ic_downvote)
                upvoteListener.onUpvoteClick(position)
            }

            downvoteButton.setOnClickListener {
                if (locations[position].voteStatus == LocationOption.VoteStatus.DOWNVOTED) {
                    downvoteButton.setImageResource(R.drawable.ic_downvote)
                } else {
                    downvoteButton.setImageResource(R.drawable.ic_downvote_clicked)
                }
                upvoteButton.setImageResource(R.drawable.ic_upvote)
                downvoteListener.onDownvoteClick(position)
            }

            locDesc.setOnClickListener {
                descriptionListener.onDescriptionClick(position)
            }
        }
    }
}
