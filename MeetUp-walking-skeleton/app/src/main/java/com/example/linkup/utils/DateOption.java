package com.example.linkup.utils;

import java.time.LocalDate;

public class DateOption {

    private final LocalDate date;
    private final int[] slotScores;
    private final int[] slotVoteStatuses;

    public DateOption(LocalDate date, int[] scores) {
        this.date = date;
        slotScores = scores;
        slotVoteStatuses = new int[24];
    }

    public DateOption(LocalDate date, int[] scores, int[] voteStatuses) {
        this.date = date;
        slotScores = scores;
        slotVoteStatuses = voteStatuses;
    }

    public LocalDate getDate() {
        return date;
    }

    public int[] getSlotScores() {
        return slotScores;
    }

    public int[] getSlotVoteStatuses() {
        return slotVoteStatuses;
    }
}
