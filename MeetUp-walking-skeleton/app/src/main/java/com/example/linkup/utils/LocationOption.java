package com.example.linkup.utils;

import org.jetbrains.annotations.NotNull;

public class LocationOption {

    private int score;
    private final String name;
    private final int locationId;
    private String description;
    @NotNull
    private VoteStatus voteStatus;

    public LocationOption(int locationId, String name, String desc, int score, @NotNull VoteStatus voteStatus) {
        this.locationId = locationId;
        this.name = name;
        this.description = desc;
        this.score = score;
        this.voteStatus = voteStatus;
    }

    @NotNull
    public VoteStatus getVoteStatus() {
        return voteStatus;
    }

    public void setVoteStatus(@NotNull VoteStatus voteStatus) {
        this.voteStatus = voteStatus;
    }

    public int getLocationId() {
        return locationId;
    }

    public enum VoteStatus {
        UPVOTED,
        DOWNVOTED,
        NEUTRAL
    }

    public LocationOption(String name, String description, int score) {
        this.locationId = -1;
        this.name = name;
        this.description = description;
        this.score = score;
        voteStatus = VoteStatus.NEUTRAL;
    }

    public LocationOption(int locationId, String name, String description, int score) {
        this.locationId = locationId;
        this.name = name;
        this.description = description;
        this.score = score;
        voteStatus = VoteStatus.NEUTRAL;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

}
