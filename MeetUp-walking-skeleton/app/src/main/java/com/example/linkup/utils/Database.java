package com.example.linkup.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Database {

    private Connection connection;

    private final String host = "db.doc.ic.ac.uk";  // For Google Cloud Postgresql
    private final String database = "g205001010_u";
    private final int port = 5432;
    private final String user = "g205001010_u";
    private final String pass = "liCKIQBiOZ";
    private String url = "jdbc:postgresql://%s:%d/%s";
    private boolean status;

    private Statement statement;

    public Database() {
        this.url = String.format(this.url, this.host, this.port, this.database);
        connect();

        try {
            this.statement = connection.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        System.out.println("connection status:" + status);
    }

    private void connect() {
        Thread thread = new Thread(() -> {
            try {
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection(url, user, pass);
                status = true;
                System.out.println("connected: true");
            } catch (Exception e) {
                status = false;
                System.out.print(e.getMessage());
                e.printStackTrace();
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (Exception e) {
            e.printStackTrace();
            this.status = false;
        }
    }

    public boolean execute(String sqlStatement) {
        try {
            statement.execute(sqlStatement);
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public List<String> retrieveUsers() {
        List<String> result = new ArrayList<>();
        String sqlStatement = "SELECT name FROM public.\"Users\"";
        try {
            ResultSet resultSet = statement.executeQuery(sqlStatement);
            while (resultSet.next()) {
                String s = resultSet.getString("name");
                result.add(s);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    public Connection getExtraConnection(){
        Connection c = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection(url, user, pass);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return c;
    }

    public ResultSet query(String sqlStatement) {
        try {
            return statement.executeQuery(sqlStatement);
        } catch (SQLException e) {
            return null;
        }
    }

    public class Query implements Runnable {

        private final String sqlStatement;
        private volatile ResultSet result;

        public Query(String sqlStatement) {
            this.sqlStatement = sqlStatement;
        }

        @Override
        public void run() {
            try {
                result = statement.executeQuery(sqlStatement);
            } catch (SQLException e) {
                result = null;
            }
        }

        public ResultSet getResult() {
            return result;
        }
    }

    public boolean getStatus() {
        return status;
    }

    public void close() {
        try {
            statement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}