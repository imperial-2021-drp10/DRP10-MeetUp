package com.example.linkup.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.linkup.BuildConfig
import com.example.linkup.R
import com.example.linkup.utils.Database
import com.example.linkup.utils.HashFunction
import com.google.android.libraries.places.api.Places


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // Initialise Google Places
        Places.initialize(applicationContext, BuildConfig.MAPS_API_KEY)

        // Get the relevant views from the layout
        val emailField =
            findViewById<EditText>(R.id.loginEmail) // emails are converted to lowercase when dealing with database
        val passwordField = findViewById<EditText>(R.id.loginPassword)

        val loginButton = findViewById<Button>(R.id.loginLoginButton)
        val registerButton = findViewById<Button>(R.id.loginRegisterButton)

        // Sets a listener on the login button to authenticate the user
        loginButton?.setOnClickListener {

            // Connect to the database
            val db = Database()

            // Set up a query
            val query = db.Query(
                """
                    SELECT *
                    FROM public."Users"
                    WHERE user_email='${emailField.text.toString().lowercase()}'
                """.trimIndent()
            )

            val loginThread = Thread(query)

            loginThread.start()
            loginThread.join()

            val queryResult = query.result

            // Checks that the query was made
            if (queryResult == null) {
                Toast.makeText(
                    this@LoginActivity,
                    "Error: Problem connecting to database",
                    Toast.LENGTH_LONG
                ).show()
            }
            // Checks if the provided email existed
            else if (!queryResult.next()) {
                Toast.makeText(
                    this@LoginActivity,
                    "Error: Account doesn't exist",
                    Toast.LENGTH_LONG
                ).show()
            }
            else {
                // Get the values from the query and move the table pointer to this single row
                val userId = queryResult.getInt("user_id")
                val userName = queryResult.getString("user_name")
                val hashedPassword = queryResult.getString("user_password")

                // Hash the given password
                val hashedPasswordInput = HashFunction.hash(passwordField.text.toString())

                // Check that the hashed password input matches the row
                if (hashedPassword != hashedPasswordInput) {
                    Toast.makeText(
                        this@LoginActivity,
                        "Error: Incorrect password",
                        Toast.LENGTH_LONG
                    ).show()
                }
                else {

                    // Prepare to switch to the main app page
                    val upcomingLinkupsIntent = Intent(this@LoginActivity, UpcomingLinkupsActivity::class.java)
                    upcomingLinkupsIntent.putExtras(intent)
                    upcomingLinkupsIntent.putExtra("USER_ID", userId)
                    upcomingLinkupsIntent.putExtra("USER_NAME", userName)
                    startActivity(upcomingLinkupsIntent)
                }
            }

        }

        // Sets a listener on the register button to redirect the user to the Register Activity
        registerButton?.setOnClickListener {
            val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
            startActivity(intent)
        }
    }
}