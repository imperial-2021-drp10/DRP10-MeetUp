package com.example.linkup.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.example.linkup.R
import com.example.linkup.utils.User


class UpcomingLinkupsActivity : AppCompatActivity() {

    private lateinit var user : User

    override fun onStart() {
        super.onStart()
        printUpcomingLinkUps(thisWeek = true, nextWeek = false)
        printUpcomingLinkUps(thisWeek = false, nextWeek = true)
        printUpcomingLinkUps(thisWeek = false, nextWeek = false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upcoming_linkups)

        // Create a User using the userId from login Activity
        user = User(intent.getIntExtra("USER_ID", -1))

        val createLinkingButton = findViewById<Button>(R.id.createLinkingButton)

        // Sets a listener on the createLinkingButton to redirect the user to the Add Participants Activity
        createLinkingButton?.setOnClickListener {
            val addParticipantsIntent = Intent(this@UpcomingLinkupsActivity, AddParticipantsActivity::class.java)
            addParticipantsIntent.putExtras(intent)
            startActivity(addParticipantsIntent)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        val thread = Thread {
            user.destroyUser()
        }
        thread.start()
    }


    @SuppressLint("InflateParams")
    private fun printUpcomingLinkUps(thisWeek: Boolean, nextWeek: Boolean) {
        var linkUpNames : List<String> = listOf()
        var linkUpIds : List<Int> = listOf()
        val namesIdsThreads = Thread {
            if (thisWeek) {
                linkUpNames = user.generateThisWeekLinkUpNames()
                linkUpIds = user.generateThisWeekLinkUpIds()
            } else if (nextWeek) {
                linkUpNames = user.generateLaterWeeksLinkUpNames()
                linkUpIds = user.generateLaterWeeksLinkUpIds()
            } else if (!nextWeek && !thisWeek) {
                linkUpNames = user.generateDateTBDLinkUpNames()
                linkUpIds = user.generateDateTBDLinkUpIds()
            }
        }
        namesIdsThreads.start()
        namesIdsThreads.join()

        val upcomingLinkUpsLinearLayout : LinearLayout
        if (thisWeek) {
            upcomingLinkUpsLinearLayout = findViewById(R.id.upcomingLinkUps)
            if (linkUpNames.isNotEmpty()) {
                upcomingLinkUpsLinearLayout.removeView(findViewById(R.id.noUpcomingLinkups))
            }
        } else if (nextWeek) {
            upcomingLinkUpsLinearLayout = findViewById(R.id.upcomingLinkUpsNextWeek)
            if (linkUpNames.isNotEmpty()) {
                upcomingLinkUpsLinearLayout.removeView(findViewById(R.id.noUpcomingLinkupsNextWeek))
            }
        } else {
            upcomingLinkUpsLinearLayout = findViewById(R.id.upcomingLinkUpsDateUndecided)
            if (linkUpNames.isNotEmpty()) {
                upcomingLinkUpsLinearLayout.removeView(findViewById(R.id.noUpcomingLinkupsDateUndecided))
            }
        }

        for (i in linkUpNames.indices) {
            var goingToLinkUp = false
            val thread = Thread {
                goingToLinkUp = user.isGoingToLinkUp(linkUpIds[i])
            }
            thread.start()
            thread.join()

            var linkUp = findViewById<Button>(linkUpIds[i])

            if (linkUp != null) {
                upcomingLinkUpsLinearLayout.removeView(linkUp)
            }

            linkUp =
                layoutInflater.inflate(R.layout.button, null) as Button
            val title = linkUpNames[i] + if (goingToLinkUp) " (Going)" else " (Not Going)"
            linkUp.text = title
            linkUp.id = linkUpIds[i]
            upcomingLinkUpsLinearLayout.addView(linkUp)
            linkUp.setOnClickListener {
                val showLinkupIntent = Intent(this@UpcomingLinkupsActivity, LinkUpActivity::class.java)
                showLinkupIntent.putExtras(intent)
                showLinkupIntent.putExtra("MEETUP_ID", linkUpIds[i])
                startActivity(showLinkupIntent)
            }


            if (goingToLinkUp) {
                linkUp.setTextColor(resources.getColor(R.color.green, null))
            } else {
                linkUp.setTextColor(resources.getColor(R.color.red, null))
            }

        }
    }
}