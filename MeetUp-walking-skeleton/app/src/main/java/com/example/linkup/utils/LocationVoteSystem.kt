package com.example.linkup.utils

import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import java.sql.SQLException

class LocationVoteSystem(
    private val scoreToWin: Int,
    private val userId: Int,
    private val linkupId: Int
) : Parcelable {

    val locations: MutableList<LocationOption>
    private val db: Database = Database()
    var voteWon: Boolean

    constructor(parcel: Parcel, scoreToWin: Int) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    )

    init {
        locations = ArrayList()
        populateLocations()
        voteWon = checkIfVoteWon()
    }

    fun getUserId(): Int {
        return userId
    }

    fun getScoreToWin(): Int {
        return scoreToWin
    }

    private fun populateLocations() {
        val getLocations = Thread {
            val sqlStatement2 = "SELECT * FROM public.\"Votes\" WHERE user_id = $userId"
            val sqlStatement =
                "SELECT * FROM public.\"Locations\" WHERE linkup_id = $linkupId ORDER BY location_score DESC"
            try {
                val voteMap = HashMap<Int, Int>()
                val voteSet = db.query(sqlStatement2)

                while (voteSet.next()) {
                    val locationId = voteSet.getInt("location_id")
                    val voteStatus = voteSet.getInt("vote_status")
                    voteMap[locationId] = voteStatus
                }

                val resultSet = db.query(sqlStatement)
                while (resultSet.next()) {
                    val locationId = resultSet.getInt("location_id")
                    val name = resultSet.getString("location_name")
                    val desc = resultSet.getString("location_desc")
                    val score = resultSet.getInt("location_score")
                    var voteStatus: LocationOption.VoteStatus = LocationOption.VoteStatus.NEUTRAL
                    if (voteMap.containsKey(locationId)) {
                        if (voteMap[locationId] == 1) {
                            Log.i("LocationVoteSystem", "Found the downvoted thing")
                            voteStatus = LocationOption.VoteStatus.DOWNVOTED
                        } else if (voteMap[locationId] == 0) {
                            voteStatus = LocationOption.VoteStatus.UPVOTED
                        }
                    }
                    locations.add(LocationOption(locationId, name, desc, score, voteStatus))
                }
            } catch (throwables: SQLException) {
                throwables.printStackTrace()
            }
        }
        getLocations.start()
        getLocations.join()
    }

    fun addUpvote(position: Int) {
        val locOption = locations[position]
        when (locOption.voteStatus) {
            LocationOption.VoteStatus.NEUTRAL -> {
                locOption.score++
                locOption.voteStatus = LocationOption.VoteStatus.UPVOTED
            }
            LocationOption.VoteStatus.UPVOTED -> {
                locOption.score--
                locOption.voteStatus = LocationOption.VoteStatus.NEUTRAL
            }
            LocationOption.VoteStatus.DOWNVOTED -> {
                locOption.score += 2
                locOption.voteStatus = LocationOption.VoteStatus.UPVOTED
            }
        }
        updateVoteOnDatabase(locOption)
        voteWon = checkIfVoteWon()
        if (voteWon) {
            updateLinkupLocationOnDatabase(locOption)
        }
    }

    private fun updateLinkupLocationOnDatabase(locOption: LocationOption) {
        val updateLocationThread = Thread {
            val sqlUpdateStatement = db.extraConnection.prepareStatement("UPDATE public.\"LinkUps\"" +
                    "SET location = ?" +
                    "WHERE linkup_id = '$linkupId'")
            sqlUpdateStatement.setString(1, locOption.name)
            sqlUpdateStatement.executeUpdate()
        }
        updateLocationThread.start()
        updateLocationThread.join()
    }

    private fun updateVoteOnDatabase(locOption: LocationOption) {
        val addVoteThread = Thread {
            val sqlUpdateLocationScore = "UPDATE public.\"Locations\"" +
                    "SET location_score = '${locOption.score}'" +
                    "WHERE location_name = '${locOption.name}'"
            db.execute(sqlUpdateLocationScore)

            val sqlSelectVote =
                "SELECT * FROM public.\"Votes\" WHERE user_id = '$userId' AND location_id = '${locOption.locationId}'"
            val voteStatus = locOption.voteStatus.ordinal
            val voteSet = db.query(sqlSelectVote)
            if (!voteSet.next()) {
                // If there is no existing vote record for this location and user, add it
                val sqlInsertVote =
                    "INSERT INTO public.\"Votes\" (user_id, location_id, vote_status) " +
                            "VALUES ('$userId', '${locOption.locationId}', '$voteStatus')"
                db.execute(sqlInsertVote)
            } else {
                val sqlUpdateVote = "UPDATE public.\"Votes\"" +
                        "SET vote_status = '$voteStatus'" +
                        "WHERE user_id = '$userId' AND location_id = '${locOption.locationId}'"
                db.execute(sqlUpdateVote)
            }
            voteSet.close()
        }
        addVoteThread.start()
        addVoteThread.join()
    }

    private fun checkIfVoteWon(): Boolean {
        for (location in locations) {
            if (location.score >= scoreToWin) {
                return true
            }
        }
        return false
    }

    fun addDownvote(position: Int) {
        val locOption = locations.get(position)
        when (locOption.voteStatus) {
            LocationOption.VoteStatus.NEUTRAL -> {
                locOption.score--
                locOption.voteStatus = LocationOption.VoteStatus.DOWNVOTED
            }
            LocationOption.VoteStatus.UPVOTED -> {
                locOption.score -= 2
                locOption.voteStatus = LocationOption.VoteStatus.DOWNVOTED
            }
            LocationOption.VoteStatus.DOWNVOTED -> {
                locOption.score++
                locOption.voteStatus = LocationOption.VoteStatus.NEUTRAL
            }
        }
        updateVoteOnDatabase(locOption)
        voteWon = checkIfVoteWon()
        if (voteWon) {
            updateLinkupLocationOnDatabase(locOption)
        }
    }

    fun addLocation(location: LocationOption): Boolean {
        Log.i("LocationVoteSystem", "Called addLocation")
        var inserted = false
        val addLocationSafe = Thread {
            val connection = db.extraConnection
            val sqlSelectCheck = connection.prepareStatement(
                "SELECT * FROM public.\"Locations\" WHERE " +
                        "linkup_id = '$linkupId' AND " +
                        "location_name = ?"
            )
            sqlSelectCheck.setString(1, location.name)
            val resultSet = sqlSelectCheck.executeQuery()
            inserted = if (resultSet.next()) {
                //i.e. we found an already existing entry for this location name
                false
            } else {
                val sqlInsertStatement = connection.prepareStatement(
                    "INSERT INTO public.\"Locations\" (linkup_id, location_name, location_desc, location_score) " +
                            "VALUES ('$linkupId', ?, ?, '0')"
                )
                sqlInsertStatement.setString(1, location.name)
                sqlInsertStatement.setString(2, location.description)
                try {
                    sqlInsertStatement.executeUpdate()
                    true
                } catch (throwables: SQLException) {
                    throwables.printStackTrace()
                    false
                }
            }
            resultSet.close()
        }
        addLocationSafe.start()
        addLocationSafe.join()
        return inserted
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(scoreToWin)
        parcel.writeInt(userId)
        parcel.writeInt(linkupId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LocationVoteSystem> {
        override fun createFromParcel(parcel: Parcel): LocationVoteSystem {
            return LocationVoteSystem(parcel.readInt(), parcel.readInt(), parcel.readInt())
        }

        override fun newArray(size: Int): Array<LocationVoteSystem?> {
            return arrayOfNulls(size)
        }
    }
}