package com.example.linkup.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import com.example.linkup.R
import com.example.linkup.utils.Database
import com.example.linkup.utils.LinkUp
import com.example.linkup.utils.User
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.util.*

class LinkUpActivity : AppCompatActivity(), OnMapReadyCallback {

    lateinit var linkUp: LinkUp
    var linkUpId: Int = -1
    lateinit var db: Database

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_linkup)

        // Get user/linkup data
        val userId = intent.getIntExtra("USER_ID", -1)
        linkUpId = intent.getIntExtra("MEETUP_ID", -1)
        linkUp = LinkUp(linkUpId)

        // Set up database connection
        db = Database()

        // Print the details of the link up
        printLinkUpName()
        printParticipants(linkUpId)
        printDetails()

        setUpSwitch(userId, linkUpId)

        val mapFragment =
            supportFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onResume() {
        super.onResume()
        linkUp.location = null
        linkUp.date = null
        linkUp.time = null
        printLinkUpName()
        printDetails()
    }

    @SuppressLint("UseSwitchCompatOrMaterialCode")
    private fun setUpSwitch(userId: Int, linkUpId: Int) {

        val switch = findViewById<Switch>(R.id.switchAttending)
        var isGoing = false

        val isGoingThread = Thread {
            val mainUser = User(userId)
            isGoing = mainUser.isGoingToLinkUp(linkUpId)
        }

        isGoingThread.start()
        isGoingThread.join()

        switch.isChecked = isGoing

        // Add listener to switch
        switch.setOnCheckedChangeListener { _, isChecked: Boolean ->
            val checkedChangeThread = Thread {
                db.execute(
                    """
                        UPDATE public."Attending"
                        SET is_going=$isChecked
                        WHERE user_id=$userId AND linkup_id=$linkUpId
                    """.trimIndent()
                )
            }
            checkedChangeThread.start()
            checkedChangeThread.join()
            Toast.makeText(this, "Attendance has been updated", Toast.LENGTH_LONG).show()
            finish()
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun chooseLocation(view: View) {
        val linkUpSize = linkUp.generateParticipantsIds().size
        val currentLocationScore = getCurrentLinkupLocationScore()

        if (currentLocationScore != linkUpSize) {
            /* Then we can still vote, as the current location did not come from a vote, or it is
               still tbd. */
            val locationIntent = Intent(this, LocationVoteActivity::class.java)
            locationIntent.putExtras(intent)
            locationIntent.putExtra("MEETUP_SIZE", linkUpSize)
            startActivity(locationIntent)
        } else {
            Toast.makeText(this, "The vote has already been won!", Toast.LENGTH_LONG).show()
        }
    }

    private fun getCurrentLinkupLocationScore(): Int {
        var result = 0
        val thread = Thread {
            val locationText = findViewById<TextView>(R.id.locationTextView).text
            /* Yes selecting by name is gross, but we have enforced that there are no duplicate
               locations in our database so this should be fine. */
            val sqlGetScore = "SELECT * FROM public.\"Locations\" WHERE " +
                    "location_name = '$locationText'"
            val resultSet = db.query(sqlGetScore)
            if (resultSet.next()) {
                result = resultSet.getInt("location_score")
            }
            resultSet.close()
        }
        thread.start()
        thread.join()
        return result
    }

    @Suppress("UNUSED_PARAMETER")
    fun chooseDateTime(view: View) {
        val linkUpSize = linkUp.generateParticipantsIds().size
        val currentLinkupTimeScore = getCurrentLinkupDateTimeScore()
        if (currentLinkupTimeScore != linkUpSize) {
            val dateTimeIntent = Intent(this, DatetimeVoteActivity::class.java)
            dateTimeIntent.putExtras(intent)
            dateTimeIntent.putExtra("MEETUP_SIZE", linkUp.generateParticipantsIds().size)
            startActivity(dateTimeIntent)
        } else {
            Toast.makeText(this, "The vote has already been won!", Toast.LENGTH_LONG).show()
        }
    }

    private fun getCurrentLinkupDateTimeScore(): Int {
        var result = 0
        val thread = Thread {
            val currentLinkupDate = linkUp.generateDate()
            if (currentLinkupDate == "TBD") {
                result = 0
            } else {
                /* Yes selecting by name is gross, but we have enforced that there are no duplicate
               locations in our database so this should be fine. */
                val sqlGetScore = "SELECT * FROM public.\"DateTimes\" WHERE " +
                        "date = '$currentLinkupDate' AND " +
                        "linkup_id = '$linkUpId'"
                val resultSet = db.query(sqlGetScore)
                if (resultSet.next()) {
                    val slotScores = IntArray(24)
                    for (i in 0..23) {
                        slotScores[i] = resultSet.getInt("slot${i}score")
                    }
                    result = Arrays.stream(slotScores).max().asInt
                }
                resultSet.close()
            }
        }
        thread.start()
        thread.join()
        return result
    }

    override fun onDestroy() {
        super.onDestroy()
        val thread = Thread {
            db.close()
            linkUp.destroyLinkUp()
        }
        thread.start()
    }

    private fun printLinkUpName() {
        val linkUpTextView = findViewById<TextView>(R.id.linkUpName)
        var linkUpName = ""
        val thread = Thread {
            linkUpName = linkUp.generateLinkUpName().toString()

        }
        thread.start()
        thread.join()

        linkUpTextView.text = linkUpName
    }

    private fun printParticipants(linkup_id: Int) {

        val goingParticipantsTextView = findViewById<TextView>(R.id.goingParticipants)
        val notGoingParticipantsTextView = findViewById<TextView>(R.id.notGoingParticipants)

        val goingChipGroup = findViewById<ChipGroup>(R.id.goingChipGroup)
        val notGoingChipGroup = findViewById<ChipGroup>(R.id.notGoingChipGroup)

        goingChipGroup.removeAllViews()
        notGoingChipGroup.removeAllViews()

        val thread = Thread {
            val participants = linkUp.generateParticipantsNames()
            if (participants != null) {

                var hasGoing = false
                var hasNotGoing = false

                // Iterates through each participant, checking if they're going
                for (i in 0 until participants.size) {

                    val user = User(linkUp.generateParticipantsIds()[i])

                    if (user.isGoingToLinkUp(linkup_id)) {
                        hasGoing = true
                        val chip = layoutInflater.inflate(
                            R.layout.layout_chip_friend,
                            goingChipGroup,
                            false
                        ) as Chip
                        chip.text = participants[i] // Display the name of the user
                        goingChipGroup.addView(chip)
                    } else {
                        hasNotGoing = true
                        val chip = layoutInflater.inflate(
                            R.layout.layout_chip_friend,
                            notGoingChipGroup,
                            false
                        ) as Chip
                        chip.text = participants[i] // Display the name of the user
                        notGoingChipGroup.addView(chip)
                    }

                    user.destroyUser()
                }

                // Disable chips from being clicked
                goingChipGroup.children.iterator().forEach {
                    val itChip = it as Chip
                    itChip.isCheckable = false
                }
                notGoingChipGroup.children.iterator().forEach {
                    val itChip = it as Chip
                    itChip.isCheckable = false
                }

                // Formats the layout appropriately
                if (!hasGoing) {
                    // Hide "Going" views
                    goingParticipantsTextView.visibility = View.GONE
                    goingChipGroup.visibility = View.GONE
                }

                if (!hasNotGoing) {
                    // Hide "Not Going" views
                    notGoingParticipantsTextView.visibility = View.GONE
                    notGoingChipGroup.visibility = View.GONE
                }
            }
        }
        thread.start()
        thread.join()

    }

    private fun printDetails() {

        val locationText = findViewById<TextView>(R.id.locationTextView)
        var location: String? = getString(R.string.Undecided)

        val locationThread = Thread {
            val queriedLocation = linkUp.generateLocation()
            if (queriedLocation != null && queriedLocation != "NULL") {
                location = queriedLocation
            }
        }
        locationThread.start()
        locationThread.join()

        locationText.text = location

        val dateTimeText = findViewById<TextView>(R.id.dateTimeTextView)
        var dateTime: String? = ""

        var queriedDate: String? = null
        val dateTimeThread = Thread {
            queriedDate = linkUp.generateDate()
        }
        dateTimeThread.start()
        dateTimeThread.join()
        if (queriedDate != null) {
            dateTime += queriedDate
        }

        val timeThread = Thread {
            val queriedTime = linkUp.generateTime()
            if (queriedTime != null) {
                dateTime += " $queriedTime"
            }
        }
        timeThread.start()
        timeThread.join()

        if (dateTime != "") {
            dateTimeText.text = dateTime
        } else {
            dateTimeText.text = getString(R.string.Undecided)
        }
    }

    override fun onMapReady(map: GoogleMap) {

        val location = findViewById<TextView>(R.id.locationTextView).text.toString()

        // Instantiate a Geocoder to convert an input to a location
        val coder = Geocoder(this)

        // Get 1 potential address from the Geocoder
        val address = coder.getFromLocationName(location, 1)

        // Convert location to LatLong
        if (address == null || address.size == 0) {
            Toast.makeText(
                this@LinkUpActivity,
                "The location couldn't be found on the map",
                Toast.LENGTH_LONG
            ).show()
        } else {

            val latLng = LatLng(address[0].latitude, address[0].longitude)
            val zoom = 12F

            // Zoom map towards location
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))

            // Add marker
            map.addMarker(
                MarkerOptions()
                    .position(latLng)
                    .title(location)
            )

        }

    }
}