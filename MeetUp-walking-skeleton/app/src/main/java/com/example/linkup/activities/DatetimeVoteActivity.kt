package com.example.linkup.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.linkup.R
import com.example.linkup.utils.CalendarAdapter
import com.example.linkup.utils.Database
import com.example.linkup.utils.DateOption
import java.time.LocalDate
import java.time.YearMonth
import java.time.format.DateTimeFormatter


class DatetimeVoteActivity : AppCompatActivity(), CalendarAdapter.OnItemListener {

    private lateinit var monthYearText: TextView
    private lateinit var calendarRecyclerView: RecyclerView
    private lateinit var selectedDate: LocalDate
    private lateinit var daysInMonth: MutableList<DateOption>
    private lateinit var calendarAdapter: CalendarAdapter

    private var linkupId = -1
    private var linkupSize = -1
    private val db = Database()

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_datetime_vote)

        linkupId = intent.getIntExtra("MEETUP_ID", -1)
        linkupSize = intent.getIntExtra("MEETUP_SIZE", -1)

        calendarRecyclerView = findViewById(R.id.calendarRecyclerView)
        monthYearText = findViewById(R.id.monthYearTV)
        selectedDate = LocalDate.now()

        val scoreToWinText = findViewById<TextView>(R.id.scoreToWinDateText)
        scoreToWinText.text = "Score to win is $linkupSize!"

        setMonthView()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onResume() {
        super.onResume()
        daysInMonth = daysInMonthArray(selectedDate)
        calendarAdapter.daysOfMonth = daysInMonth
        calendarAdapter.notifyDataSetChanged()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setMonthView() {
        monthYearText.text = monthYearFromDate(selectedDate)

        daysInMonth = daysInMonthArray(selectedDate)

        calendarAdapter = CalendarAdapter(daysInMonth, this)
        val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(applicationContext, 7)
        calendarRecyclerView.layoutManager = layoutManager
        calendarRecyclerView.adapter = calendarAdapter
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun daysInMonthArray(date: LocalDate): ArrayList<DateOption> {
        val daysInMonthArray = ArrayList<DateOption>()
        val yearMonth: YearMonth = YearMonth.from(date)
        val daysInMonth: Int = yearMonth.lengthOfMonth()
        val firstOfMonth: LocalDate = selectedDate.withDayOfMonth(1)

        val dayOfWeek = firstOfMonth.dayOfWeek.value
        for (i in 1..42) {
            if (i <= dayOfWeek || i > daysInMonth + dayOfWeek) {
                daysInMonthArray.add(DateOption(null, null))
            } else {
                var str1 = selectedDate.month.value.toString()
                var str2 = (i - dayOfWeek).toString()
                if (str1.length == 1) {
                    str1 = "0$str1"
                }
                if (str2.length == 1) {
                    str2 = "0$str2"
                }
                val dateText: String = selectedDate.year.toString() + "-" + str1 + "-" + str2
                val scoreArray = getScoreArrayForISODate(dateText)
                val dateToAdd: LocalDate = LocalDate.parse(dateText)
                daysInMonthArray.add(DateOption(dateToAdd, scoreArray))
            }
        }
        return daysInMonthArray
    }

    private fun getScoreArrayForISODate(dateText: String): IntArray {
        val result = IntArray(24)
        val getScores = Thread {
            val sqlQuery =
                "SELECT * FROM public.\"DateTimes\" WHERE linkup_id = $linkupId AND date = '$dateText'"
            val resultSet = db.query(sqlQuery)
            while (resultSet.next()) {
                for (i in 0..23) {
                    result[i] = resultSet.getInt("slot" + i + "score")
                }
            }
            resultSet.close()
        }
        getScores.start()
        getScores.join()
        return result
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun monthYearFromDate(date: LocalDate): String? {
        val formatter = DateTimeFormatter.ofPattern("MMMM yyyy")
        return date.format(formatter)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @Suppress("UNUSED_PARAMETER")
    fun previousMonthAction(view: View) {
        selectedDate = selectedDate.minusMonths(1)
        setMonthView()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @Suppress("UNUSED_PARAMETER")
    fun nextMonthAction(view: View) {
        selectedDate = selectedDate.plusMonths(1)
        setMonthView()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onItemClick(position: Int, dayText: String) {
        if (dayText != "") {
//            val message = "Selected Date " + dayText + " " + monthYearFromDate(selectedDate)
//            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
            val dateClicked = daysInMonth[position].date.toString()
            val intent = Intent(this, SelectTimeslotActivity::class.java).apply {
                putExtra("DATE_CLICKED", dateClicked)
                putExtra("MEETUP_SIZE", linkupSize)
                putExtras(intent)
            }
            startActivity(intent)
        }
    }


}