package com.example.linkup.activities

import android.annotation.SuppressLint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.linkup.R
import com.example.linkup.utils.Database
import com.example.linkup.utils.DateOption
import com.example.linkup.utils.TimeslotListAdapter
import java.time.LocalDate
import java.util.*

class SelectTimeslotActivity : AppCompatActivity(), TimeslotListAdapter.TimeslotListener {

    private lateinit var timeslotRV: RecyclerView
    private lateinit var timeslotListAdapter: TimeslotListAdapter
    private lateinit var dateOption: DateOption

    private var userId = -1
    private var linkupId = -1
    private var linkupSize = -1
    private var dateClicked: String? = null
    private val db = Database()

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_timeslot)

        userId = intent.getIntExtra("USER_ID", -1)
        linkupId = intent.getIntExtra("MEETUP_ID", -1)
        dateClicked = intent.getStringExtra("DATE_CLICKED")
        linkupSize = intent.getIntExtra("MEETUP_SIZE", -1)

        timeslotRV = findViewById(R.id.timeslotRV)
        val chooseATimeOnTV = findViewById<TextView>(R.id.chooseATimeOnTV)
        chooseATimeOnTV.text = "Choose a time on $dateClicked"

        val scoreToWinText = findViewById<TextView>(R.id.scoreToWinTimeText)

        dateOption = dateClicked?.let { generateDateOptionFromDate(it) }!!
        if (Arrays.stream(dateOption.slotScores).max().asInt != linkupSize) {
            scoreToWinText.text = "Score to win is $linkupSize!"
        } else {
            scoreToWinText.text = "Vote won!!!"
        }
        timeslotListAdapter = TimeslotListAdapter(dateOption, this)
        timeslotRV.adapter = timeslotListAdapter
        timeslotRV.layoutManager = LinearLayoutManager(this)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun generateDateOptionFromDate(dateClicked: String): DateOption {
        val scoreArray = IntArray(24)
        val getDayRow = Thread {
            val sqlQuery =
                "SELECT * FROM public.\"DateTimes\" WHERE linkup_id = $linkupId AND date = '$dateClicked'"
            val resultSet = db.query(sqlQuery)
            while (resultSet.next()) {
                for (i in 0..23) {
                    scoreArray[i] = resultSet.getInt("slot" + i + "score")
                }
            }
        }
        getDayRow.start()
        getDayRow.join()

        val voteStatusArray = IntArray(24)
        val getVoteStatuses = Thread {
            val sqlSelectVoteStatuses =
                "SELECT * FROM public.\"DateTimeVotes\" WHERE linkup_id = '$linkupId' AND " +
                        "date = '$dateClicked' AND " +
                        "user_id = '$userId'"
            val resultSet = db.query(sqlSelectVoteStatuses)
            while (resultSet.next()) {
                for (i in 0..23) {
                    voteStatusArray[i] = resultSet.getInt("slot" + i + "status")
                }
            }
        }
        getVoteStatuses.start()
        getVoteStatuses.join()

        return DateOption(LocalDate.parse(dateClicked), scoreArray, voteStatusArray)
    }

    @Suppress("UNUSED_PARAMETER")
    fun freeAllDay(view: View) {
        for (i in 0..23) {
            if (dateOption.slotVoteStatuses[i] == 0) {
                onTimeslotClick(i)
            }
        }
    }

    override fun onTimeslotClick(position: Int) {
        Log.i("SelectTimeslotActivity", "clicked")

        if (dateOption.slotVoteStatuses[position] == 0) {
            dateOption.slotScores[position]++
            dateOption.slotVoteStatuses[position] = 1
        } else {
            dateOption.slotScores[position]--
            dateOption.slotVoteStatuses[position] = 0
        }

        val updateScoreThread = Thread {
            val sqlSelectDay =
                "SELECT * FROM public.\"DateTimes\" WHERE linkup_id = '$linkupId' AND date = '${dateOption.date}'"
            val voteSet = db.query(sqlSelectDay)
            if (!voteSet.next()) {
                // If there is no existing vote record for this day and user, add it
                val sqlInsertVote =
                    "INSERT INTO public.\"DateTimes\" (linkup_id, date, slot${position}score) " +
                            "VALUES ('$linkupId', '${dateOption.date}', '${dateOption.slotScores[position]}')"
                db.execute(sqlInsertVote)
            } else {
                val sqlUpdateTimeslotScore = "UPDATE public.\"DateTimes\"" +
                        "SET slot${position}score = '${dateOption.slotScores[position]}'" +
                        "WHERE linkup_id = '${linkupId}' AND " +
                        "date = '${dateOption.date}'"
                db.execute(sqlUpdateTimeslotScore)
            }
        }
        updateScoreThread.start()
        updateScoreThread.join()

        val updateVoteStatusThread = Thread {
            val sqlSelectDay =
                "SELECT * FROM public.\"DateTimeVotes\" WHERE user_id = '$userId' AND " +
                        "linkup_id = '$linkupId' AND " +
                        "date = '${dateOption.date}'"
            val voteSet = db.query(sqlSelectDay)
            if (!voteSet.next()) {
                // If there is no existing vote record for this day and user, add it
                val sqlInsertVote =
                    "INSERT INTO public.\"DateTimeVotes\" (user_id, linkup_id, date, slot${position}status) " +
                            "VALUES ('$userId', '$linkupId', '${dateOption.date}', '${dateOption.slotVoteStatuses[position]}')"
                db.execute(sqlInsertVote)
            } else {
                val sqlUpdateVoteStatus = "UPDATE public.\"DateTimeVotes\"" +
                        "SET slot${position}status = '${dateOption.slotVoteStatuses[position]}'" +
                        "WHERE linkup_id = '${linkupId}' AND " +
                        "date = '${dateOption.date}' AND " +
                        "user_id = '$userId'"
                db.execute(sqlUpdateVoteStatus)
            }
        }
        updateVoteStatusThread.start()
        updateVoteStatusThread.join()

        if (dateOption.slotScores[position] == linkupSize) {
            //Vote has been won, so update date and time in database
            val updateLinkupDateTimeThread = Thread {
                val timeString = if (position <= 9) {
                    "0"
                } else {
                    ""
                } + position + ":00:00"
                val sqlUpdateLinkupDateTime = "UPDATE public.\"LinkUps\"" +
                        "SET date = '${dateOption.date}', " +
                        "time = '$timeString'" +
                        "WHERE linkup_id = '$linkupId'"
                db.execute(sqlUpdateLinkupDateTime)
            }
            updateLinkupDateTimeThread.start()
            updateLinkupDateTimeThread.join()

            val scoreToWinText = findViewById<TextView>(R.id.scoreToWinTimeText)
            scoreToWinText.text = "Vote won!!!"
        } else {
            val scoreToWinText = findViewById<TextView>(R.id.scoreToWinTimeText)
            scoreToWinText.text = "Score to win is $linkupSize!"
        }

        timeslotListAdapter.notifyDataSetChanged()
    }
}