package com.example.linkup.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.linkup.R
import com.example.linkup.utils.Database
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import java.sql.Statement
import java.time.LocalDate
import java.time.LocalTime
import java.util.*

class CreateLinkUpActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {

    private lateinit var db : Database
    private var userId : Int = -1
    private lateinit var participantIds : ArrayList<Int>

    // All null by default
    private var linkUpLocation : String? = null
    private var linkUpDate : LocalDate?  = null
    private var linkUpTime : LocalTime?  = null

    // Date and time fields
    private var day = 0
    private var month = 0
    private var year = 0
    private var hour = 0
    private var minute = 0

    // Fields for buttons/views, initialised in onCreate()
    private lateinit var linkUpNameInput : TextView

    private lateinit var locationTitle : TextView
    private lateinit var locationVote  : Button
    private lateinit var locationInput : Button

    private lateinit var dateTitle : TextView
    private lateinit var dateVote  : Button
    private lateinit var dateInput : Button

    private lateinit var timeTitle : TextView
    private lateinit var timeVote  : Button
    private lateinit var timeInput : Button

    // Default colours
    private val selected   = Color.parseColor("#11f443") // Green
    private val unselected = Color.parseColor("#ff6624") // Orange

    private val AUTOCOMPLETE_REQUEST_CODE = 1

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_linkup)

        // Initialise fields
        linkUpNameInput = findViewById(R.id.linkUpName)

        locationTitle = findViewById(R.id.linkUpLocation)
        locationVote  = findViewById(R.id.voteLocation)
        locationInput = findViewById(R.id.selectLocation)

        dateTitle = findViewById(R.id.linkUpDate)
        dateVote  = findViewById(R.id.voteDate)
        dateInput = findViewById(R.id.selectDate)

        timeTitle = findViewById(R.id.linkUpTime)
        timeVote  = findViewById(R.id.voteTime)
        timeInput = findViewById(R.id.selectTime)

        userId = intent.getIntExtra("USER_ID", -1)
        participantIds = intent.getIntegerArrayListExtra("PARTICIPANT_IDS") as ArrayList<Int>

        // Set background colours to green
        locationVote.setBackgroundColor(selected)
        dateVote.setBackgroundColor(selected)
        timeVote.setBackgroundColor(selected)

        // Update titles
        val locationText = "${getString(R.string.locationTitleDefault)}: ${getString(R.string.voteText)}"
        locationTitle.text = locationText
        val dateText = "${getString(R.string.dateTitleDefault)}: ${getString(R.string.voteText)}"
        dateTitle.text = dateText
        val timeText = "${getString(R.string.timeTitleDefault)}: ${getString(R.string.voteText)}"
        timeTitle.text = timeText

        // Connect to database
        db = Database()

    }

    /**
     * Used by the location vote button
     */
    fun voteLocationOnClick(view: View) {
        if (view is Button) {
            view.setBackgroundColor(selected) // Make the button green
            locationInput.setBackgroundColor(unselected)
            // Set the linkUpLocation to null and clear the Location Input
            linkUpLocation = null
            // Update the title
            val locationText = "${getString(R.string.locationTitleDefault)}: ${getString(R.string.voteText)}"
            locationTitle.text = locationText
        }
    }

    /**
     * Used by the select location button
     */
    @Suppress("UNUSED_PARAMETER")
    fun selectLocationOnClick(view: View) {

        val fields = listOf(Place.Field.ID, Place.Field.NAME)

        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)

    }

    /**
     * Used by the date vote button
     */
    fun voteDateOnClick(view: View) {
        if (view is Button) {
            view.setBackgroundColor(selected) // Make the button green
            // Set the linkUpDate to null and unhighlight the corresponding button
            linkUpDate = null
            dateInput.setBackgroundColor(unselected)
            // Update the title
            val dateText = "${getString(R.string.dateTitleDefault)}: ${getString(R.string.voteText)}"
            dateTitle.text = dateText
        }
    }

    /**
     * Used by the select date button
     */
    fun selectDateOnClick(view: View) {
        if (view is Button) {
            updateDateTimeFields()
            DatePickerDialog(this, this, year, month, day).show()
        }
    }

    /**
     * Used by the vote time button
     */
    fun voteTimeOnClick(view: View) {
        if (view is Button) {
            view.setBackgroundColor(selected) // Make the button green
            // Set the linkUpTime to null and unhighlight the corresponding button
            linkUpTime = null
            timeInput.setBackgroundColor(unselected)
            // Update the title
            val timeText = "${getString(R.string.timeTitleDefault)}: ${getString(R.string.voteText)}"
            timeTitle.text = timeText
        }
    }

    /**
     * Used by the select time button
     */
    fun selectTimeOnClick(view: View) {
        if (view is Button) {
            updateDateTimeFields()
            TimePickerDialog(this, this, hour, minute, true).show()
        }
    }

    /**
     * Used by the "Create" button
     */
    @Suppress("UNUSED_PARAMETER")
    fun createButtonOnClick(view: View) {

        val linkUpName = linkUpNameInput.text.toString()

        if (linkUpName.isBlank()) {
            Toast.makeText(this@CreateLinkUpActivity, "A name must be specified", Toast.LENGTH_LONG).show()
        }
        else {
            val locationEntry = if (linkUpLocation == null) "NULL" else linkUpLocation.toString()
            val dateEntry     = if (linkUpDate == null)     "NULL" else "'${linkUpDate.toString()}'"
            val timeEntry     = if (linkUpTime == null)     "NULL" else "'${linkUpTime.toString()}'"

            val thread = Thread {
                Log.i("CreateLinkupActivity", "Adding to database via INSERT")

                val connection = db.extraConnection
                val statement = connection.prepareStatement(
                    """
                        INSERT INTO public."LinkUps" (linkup_name, location, date, time)
                        VALUES (?, ?, $dateEntry, $timeEntry)
                    """.trimIndent(),
                    Statement.RETURN_GENERATED_KEYS
                )
                statement.setString(1, linkUpName)
                statement.setString(2, locationEntry)

                statement.executeUpdate()
                val resultSet = statement.generatedKeys
                resultSet.next()
                val linkupId = resultSet.getInt("linkup_id")
                resultSet.close()
                Log.i("CreateLinkupActivity", "Found linkupId: $linkupId")

                db.execute(
                    """
                        INSERT INTO public."Attending" (user_id, linkup_id, is_going, is_admin)
                        VALUES ('$userId', '$linkupId', 'true', 'true')
                    """.trimIndent()
                )
                print(participantIds)
                for (participantId in participantIds) {
                    db.execute( """
                        INSERT INTO public."Attending" (user_id, linkup_id, is_going, is_admin)
                        VALUES ('$participantId', '$linkupId', 'true', 'false')
                    """.trimIndent())
                }

            }

            thread.start()
            thread.join()

            val upcomingLinkupsIntent =
                Intent(this, UpcomingLinkupsActivity::class.java).apply {
                    putExtras(intent)
                }
            startActivity(upcomingLinkupsIntent)

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        val locationText = "${getString(R.string.locationTitleDefault)}: ${place.name}"
                        locationTitle.text = locationText
                        locationInput.setBackgroundColor(selected)
                        locationVote.setBackgroundColor(unselected)
                        linkUpLocation = place.name
                        Toast.makeText(this@CreateLinkUpActivity, "Location has been set", Toast.LENGTH_LONG).show()
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    Toast.makeText(this@CreateLinkUpActivity, "Error: Issue retrieving location", Toast.LENGTH_LONG).show()
                }
                Activity.RESULT_CANCELED -> {
                    Toast.makeText(this@CreateLinkUpActivity, "Location selection cancelled", Toast.LENGTH_LONG).show()
                }
            }
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        linkUpDate = LocalDate.of(year, month + 1, dayOfMonth)
        updateDateTimeFields()

        // Change buttons upon completion
        dateInput.setBackgroundColor(selected)
        dateVote.setBackgroundColor(unselected)

        // Update text shown
        val dateText = "${getString(R.string.dateTitleDefault)}: ${linkUpDate.toString()}"
        dateTitle.text = dateText
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        linkUpTime = LocalTime.of(hourOfDay, minute, 0)
        updateDateTimeFields()

        // Change buttons upon completion
        timeInput.setBackgroundColor(selected)
        timeVote.setBackgroundColor(unselected)

        // Update text shown
        val timeText = "${getString(R.string.timeTitleDefault)}: ${linkUpTime.toString()}"
        timeTitle.text = timeText
    }

    /**
     * Gets the calendar and sets the fields to correspond with it
     */
    private fun updateDateTimeFields() {
        val cal = Calendar.getInstance()
        day = cal.get(Calendar.DAY_OF_MONTH)
        month = cal.get(Calendar.MONTH)
        year = cal.get(Calendar.YEAR)
        hour = cal.get(Calendar.HOUR)
        minute = cal.get(Calendar.MINUTE)
    }


}