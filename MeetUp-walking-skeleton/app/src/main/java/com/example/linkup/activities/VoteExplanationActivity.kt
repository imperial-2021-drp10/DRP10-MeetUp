package com.example.linkup.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.widget.TextView
import androidx.core.text.bold
import com.example.linkup.R

class VoteExplanationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vote_explanation)
        setExplanationText()
    }

    private fun setExplanationText() {
        val s = SpannableStringBuilder()
            .bold { append("Voting explained\n") }
            .append(
                """
                    Vote on your favourite location from the list by pressing the upvote (up arrow button) to increase the score, or downvote (down arrow button) to decrease the score.
                    A location's score is displayed on the left, which will be locked in when it links the score to win.
                """.trimIndent()
            )
        findViewById<TextView>(R.id.explanationTextView).text = s

        val s2 = SpannableStringBuilder()
            .bold { append("Adding to the list\n") }
            .append("Anyone can add a location to the list by pressing the orange \"Add an option\" button")

        findViewById<TextView>(R.id.explanation2TextView).text = s2
    }
}