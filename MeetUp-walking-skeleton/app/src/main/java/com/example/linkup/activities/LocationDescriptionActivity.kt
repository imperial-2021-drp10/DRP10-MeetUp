package com.example.linkup.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.linkup.R
import com.example.linkup.utils.Database

class LocationDescriptionActivity : AppCompatActivity() {

    private lateinit var descBox: EditText
    private val db = Database()
    private var locationID = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_description)
        descBox = findViewById(R.id.descBox)

        locationID = intent.getIntExtra("LOCATION_ID", -1)
        fillDescription()
    }

    private fun fillDescription() {
        val thread = Thread {
            val sqlStatement =
                "SELECT location_desc FROM public.\"Locations\" WHERE location_id = $locationID"
            val resultSet = db.query(sqlStatement)
            while (resultSet.next()) {
                descBox.setText(resultSet.getString("location_desc"))
            }
        }
        thread.start()
        thread.join()
    }

    fun saveDescription(view: View) {
        if (view is Button) {
            val thread = Thread {
                val text = descBox.text.toString()
                val sqlStatement =
                    "UPDATE public.\"Locations\" SET location_desc = '$text' WHERE location_id = $locationID"
                    db.execute(sqlStatement)
            }
            thread.start()
            thread.join()
        }
        val descIntent = Intent(this, LocationVoteActivity::class.java).apply {
            putExtras(intent)
        }
        startActivity(descIntent)
        finish()
    }
}