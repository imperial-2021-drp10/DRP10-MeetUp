package com.example.linkup.activities

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import com.example.linkup.R
import com.example.linkup.utils.Database
import com.example.linkup.utils.User
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup

class AddParticipantsActivity : AppCompatActivity() {

    private lateinit var user : User
    private var invitedEmails : MutableList<String> = ArrayList()
    private val db = Database()
    private lateinit var createLinkupIntent : Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_participants)
        createLinkupIntent = Intent(this@AddParticipantsActivity, CreateLinkUpActivity::class.java)

        user = User(intent.getIntExtra("USER_ID", -1))
    }

    override fun onStart() {
        super.onStart()
        var friendsIds = arrayOf<Int>()
        var friendsNames = arrayOf<String>()
        val thread = Thread {
            friendsIds = user.generateFriendsIds().toTypedArray()
            friendsNames = user.generateFriendsNames().toTypedArray()
        }
        thread.start()
        thread.join()

        val friendsLinearLayout = findViewById<LinearLayout>(R.id.inviteFriendsLinearLayout)
        if (friendsIds.isNotEmpty()) {
            friendsLinearLayout.removeView(findViewById(R.id.noFriendsTextView))
        }

        // Iterate through each friend and add a chip for them
        val friendsChipGroup = findViewById<ChipGroup>(R.id.friendsChipGroup)

        friendsChipGroup.removeAllViews()

        for (i in friendsIds.indices) {
            val chip = layoutInflater.inflate(R.layout.layout_chip_friend, friendsChipGroup, false) as Chip
            chip.id = friendsIds[i] // Sets chip ID to the User ID
            chip.text = friendsNames[i] // Display the name of the user
            friendsChipGroup.addView(chip)
        }

    }

    /**
     * Redirect the user to the Create LinkUp Activity
     */
    @Suppress("UNUSED_PARAMETER")
    fun nextButtonOnClick(view: View) {
        // Add a list of participant IDs from the ChipGroup
        val participantIds = findViewById<ChipGroup>(R.id.friendsChipGroup).checkedChipIds as ArrayList<Int>
        createLinkupIntent.putExtra("PARTICIPANT_IDS", participantIds)

        // Put the rest of the intent in
        createLinkupIntent.putExtras(intent)

        startActivity(createLinkupIntent)
    }

    /**
     * Handles inviting a user
     */
    @Suppress("UNUSED_PARAMETER")
    fun addEmailOnClick(view: View) {

        val emailAddress = findViewById<EditText>(R.id.addEmailEditText)

        val toAddress = emailAddress.text
        if (invitedEmails.contains(toAddress.toString())) {
            /* Participant has already been added to this link up */
            Toast.makeText(this@AddParticipantsActivity,
                "You've already invited them!", Toast.LENGTH_LONG).show()
        } else {
            val subject = "You've been invited to a link up!"
            var message = ""
            var displayName = "User name"
            var participantId = -1

            var userRegistered = false
            val thread = Thread {
                val sqlQuery =
                    """
                        SELECT user_id, user_name
                        FROM public."Users"
                        WHERE user_email = '$toAddress'
                    """.trimIndent()
                val rs = db.query(sqlQuery)
                if (!rs.next()) {
                    /* User is not registered on LinkUp */
                    message = user.generateUserName() + " wants to add you as a friend! Join" +
                            " now!"
                    userRegistered = false
                } else {
                    userRegistered = true
                    displayName = rs.getString("user_name")
                    participantId = rs.getInt("user_id")
                }
            }
            thread.start()
            thread.join()

            invitedEmails.add(toAddress.toString())

            if (userRegistered) {
                val addFriendThread = Thread {
                    user.addFriend(participantId)
                }
                addFriendThread.start()
                addFriendThread.join()

                // Add the button and check it
                val friendsChipGroup = findViewById<ChipGroup>(R.id.friendsChipGroup)

                if (participantId in friendsChipGroup.children.map {it.id}) {
                    Toast.makeText(this@AddParticipantsActivity,
                        "$displayName is already your friend!", Toast.LENGTH_LONG).show()

                } else {
                    val friendsLinearLayout = findViewById<LinearLayout>(R.id.inviteFriendsLinearLayout)
                    val noFriendsView = findViewById<TextView>(R.id.noFriendsTextView)
                    if (noFriendsView != null) {
                        friendsLinearLayout.removeView(noFriendsView)
                    }
                    val chip = layoutInflater.inflate(R.layout.layout_chip_friend, friendsChipGroup, false) as Chip
                    chip.id = participantId // Sets chip ID to the User ID
                    chip.text = displayName // Display the name of the user
                    chip.isChecked = true
                    friendsChipGroup.addView(chip)
                    Toast.makeText(this@AddParticipantsActivity,
                        "$displayName added to your friends list!", Toast.LENGTH_LONG).show()
                }

            } else {
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(toAddress.toString()))
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
                emailIntent.putExtra(Intent.EXTRA_TEXT, message)
                emailIntent.type = "message/rfc822"

                try {
                    startActivity(
                        Intent.createChooser(
                            emailIntent,
                            "Send email using..."
                        )
                    )
                    finish()
                    Toast.makeText(
                        this@AddParticipantsActivity,
                        "Send an invite!",
                        Toast.LENGTH_LONG
                    ).show()
                } catch (ex: ActivityNotFoundException) {
                    Toast.makeText(
                        this@AddParticipantsActivity,
                        "No email clients installed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

//    private fun addFriendButton(displayName : String, userId : Int) {
//        val friendsLinearLayout = findViewById<LinearLayout>(R.id.inviteFriendsLinearLayout)
//        val noFriendsView = findViewById<TextView>(R.id.noFriendsTextView)
//        if (noFriendsView != null) {
//            friendsLinearLayout.removeView(noFriendsView)
//        }
//        var friend = findViewById<Button>(userId)
//        if (friend == null) {
//            friend =
//                layoutInflater.inflate(R.layout.button, null) as Button
//            "Invite $displayName".also { friend.text = it }
//            friend.id = userId
//            friendsLinearLayout.addView(friend)
//            friend.setOnClickListener {
//                if (participantIds.contains(userId)) {
//                    Toast.makeText(this@AddParticipantsActivity,
//                        "You've already added them to this link up!", Toast.LENGTH_LONG).show()
//                } else {
//                    val participantsLinearLayout = findViewById<LinearLayout>(R.id.inviteFriendsLinearLayout)
//                    participantsLinearLayout.removeView(findViewById<TextView>(R.id.addFriends))
//                    val participantAdded = TextView(this)
//                    participantIds.add(userId)
//                    participantAdded.text = displayName
//                    participantsLinearLayout.addView(participantAdded)
//                }
//            }
//        }
//    }

}
