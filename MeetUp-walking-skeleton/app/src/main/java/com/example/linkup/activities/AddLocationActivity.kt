package com.example.linkup.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.linkup.R
import com.example.linkup.utils.LocationOption
import com.example.linkup.utils.LocationVoteSystem
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode

class AddLocationActivity : AppCompatActivity() {

    private val AUTOCOMPLETE_REQUEST_CODE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_location)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val locationNameEditText = findViewById<EditText>(R.id.locNameInput)
                        val place = Autocomplete.getPlaceFromIntent(data)
                        locationNameEditText.setText(place.name)
                        Toast.makeText(this@AddLocationActivity, "Location has been set", Toast.LENGTH_LONG).show()
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    Toast.makeText(this@AddLocationActivity, "Error: Issue retrieving location", Toast.LENGTH_LONG).show()
                }
                Activity.RESULT_CANCELED -> {
                    Toast.makeText(this@AddLocationActivity, "Location selection cancelled", Toast.LENGTH_LONG).show()
                }
            }
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * Used by the select location button
     */
    @Suppress("UNUSED_PARAMETER")
    fun findLocationOnClick(view: View) {

        val fields = listOf(Place.Field.ID, Place.Field.NAME)

        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)

    }

    @Suppress("UNUSED_PARAMETER")
    fun addOptionToVote(view: View) {

        val locationVoteSystem = intent.getParcelableExtra<LocationVoteSystem>("VOTE_SYSTEM")

        val locNameInput = findViewById<EditText>(R.id.locNameInput).text.toString()
        val locDescInput = findViewById<EditText>(R.id.locDescInput).text.toString()

        if (!locationVoteSystem?.addLocation(LocationOption(locNameInput, locDescInput, 0))!!) {
            Toast.makeText(this, "Location already exists in system!", Toast.LENGTH_LONG).show()
        } else {
            val intent = Intent(this, LocationVoteActivity::class.java).apply {
                putExtra("VOTE_SYSTEM", locationVoteSystem)
                putExtras(intent)
            }
            startActivity(intent)
            finish()
        }
    }
}