package com.example.linkup.utils;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

public class LinkUp {
    private List<Integer> participantsIds;
    private List<String> participantsNames;
    private final int linkup_id;
    private String linkUpName;
    public String location;
    public String date;
    public String time;
    private final Database db;

    public LinkUp(int linkup_id) {
        this.linkup_id = linkup_id;
        this.db = new Database();
    }

    public String generateLinkUpName() {

        if (linkUpName == null) {

            ResultSet resultSet = db.query("SELECT linkup_name FROM public.\"LinkUps\" WHERE linkup_id = " + linkup_id);

            try {
                resultSet.next();
                this.linkUpName = resultSet.getString("linkup_name");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } finally {
                if (resultSet != null) {
                    try {
                        resultSet.close();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        }

        return linkUpName;

    }

    public List<Integer> generateParticipantsIds() {

        if (participantsIds == null) {
            participantsIds = new ArrayList<>();
            ResultSet resultSet = null;
            String sqlStatement = "SELECT user_id FROM public.\"Attending\" WHERE linkup_id = " + linkup_id;

            try {
                resultSet = db.query(sqlStatement);
                while (resultSet.next()) {
                    int i = resultSet.getInt("user_id");
                    participantsIds.add(i);
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } finally {
                if (resultSet != null) {
                    try {
                        resultSet.close();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        }
        return participantsIds;
    }

    public List<String> generateParticipantsNames() {

        if (participantsNames == null) {

            participantsNames = new ArrayList<>();
            List<Integer> participantsIds = generateParticipantsIds();

            for (Integer user_id : participantsIds) {
                String sqlStatement = "SELECT user_name FROM public.\"Users\" WHERE user_id = " + user_id;
                ResultSet resultSet = null;

                try {
                    resultSet = db.query(sqlStatement);
                    resultSet.next();
                    String user_name = resultSet.getString("user_name");
                    participantsNames.add(user_name);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                } finally {
                    if (resultSet != null) {
                        try {
                            resultSet.close();
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    }
                }
            }
        }
        return participantsNames;
    }

    public String generateLocation() {
        if (location == null) {
            ResultSet resultSet = db.query("SELECT location FROM public.\"LinkUps\" WHERE linkup_id = " + linkup_id);
            try {
                resultSet.next();
                this.location = resultSet.getString("location");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            finally {
                if (resultSet != null) {
                    try {
                        resultSet.close();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        }
        return location;
    }

    public String generateDate() {
        if (date == null) {
            ResultSet resultSet;
            resultSet = db.query("SELECT * FROM public.\"LinkUps\" WHERE linkup_id = " + linkup_id);
            try {
                resultSet.next();
                Date retrievedDate = resultSet.getDate("date");
//                this.date = resultSet.getDate("date").toString();
                date = (retrievedDate == null) ? "TBD" : retrievedDate.toString();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } finally {
                if (resultSet != null) {
                    try {
                        resultSet.close();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        }
        return date;
    }

    public String generateTime() {

        ResultSet resultSet;

        if (time == null) {
            resultSet = db.query("SELECT time FROM public.\"LinkUps\" WHERE linkup_id = " + linkup_id);
            try {
                resultSet.next();
//                this.time = resultSet.getTime("time").toString();
                Time retrievedTime = resultSet.getTime("time");
                this.time = (retrievedTime == null) ? "TBD" : retrievedTime.toString();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } finally {
                if (resultSet != null) {
                    try {
                        resultSet.close();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        }

        return time;

    }

    public void destroyLinkUp() {
        db.close();
    }

}
