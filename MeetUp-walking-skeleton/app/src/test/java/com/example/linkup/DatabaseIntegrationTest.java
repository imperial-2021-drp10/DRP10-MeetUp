package com.example.linkup;

import com.example.linkup.utils.Database;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DatabaseIntegrationTest {

    @Test
    public void canConnectToDatabase() {
        Database db = new Database();
        assertTrue(db.getStatus());
    }

}
